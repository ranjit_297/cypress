const { defineConfig } = require("cypress");

module.exports = defineConfig({
  watchForFileChanges: false,
  "defaultCommandTimeout": 20000 ,
  "pageLoadTimeout": 20000,
  "requestTimeout": 5000,
  viewportWidth: 1920,
  viewportHeight: 1080,
  e2e: {
    // baseUrl:"https://www.yatra.com/flights",
    setupNodeEvents(on, config) {
      
      // implement node event listeners here
    },
    specPattern: [
      // All Specs
      'cypress/tests/**/*.ts',
    ]
  },

});
