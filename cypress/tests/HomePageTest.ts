
import webElement from '../fixtures/webelement.json'
import testData from '../fixtures/testdata.json'
//test suite
describe('Home Test Suite', () => {

  beforeEach(() => {
    cy.visit('  https://www.dmartindia.com/')


  })


  //test
  it('Home Test', () => {



    cy.get(webElement.homepage.homePageFooterLinks).each((footerLinkElement) => {

      //////////////////////////////////////////////////////////////////////////////////////////////
      var footerLinkText = footerLinkElement.text().toLowerCase().replaceAll(" ","-")
      cy.wrap(footerLinkElement).click()
      cy.url().should('contain',footerLinkText)
      cy.go('back')
     //////////////////////////////////////////////////////////////////////////////////////////////

    })

  });

})
