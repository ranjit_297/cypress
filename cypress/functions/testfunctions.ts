
import webElement from '../fixtures/webelement.json'

//in function  we will pass Arguments in the fucntion signature and same variables we will refer or call where ever we need 
export const login = (userName, pwd) => {

    cy.get(webElement.themeForestPage.signInLink).click()
    cy.get(webElement.themeForestPage.usernameTextbox).type(userName)
    cy.get(webElement.themeForestPage.pwdTextbox).type(pwd)
    cy.get('button').contains('Sign in').click()

}

export const verifyLogin = () => {


    //true or false 
    //if user name texbox is still visible then login is failed 
    cy.get(webElement.themeForestPage.usernameTextbox).then(userNameTextBox => {
        var textBoxLength = userNameTextBox.length
        cy.log(textBoxLength + "")

        if (textBoxLength === 1) {
            cy.log("login is failed in if")

        } else {
            cy.log("login is passed in else")
            //assert
        }


    })



}